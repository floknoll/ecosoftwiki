﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECOSoft.ECOSoftWiki.Service
{
    public class ECOSoftWikiUser
    {

        /// <summary>
        /// Sparepartuser durch Login
        /// </summary>
        /// <param name="dc"></param>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static ECOSoftWikiUser Login(ECOSoftWikiDataContext dc, string Username, string Password, string remoteAdr, string userAgent)
        {
            Users user = dc.Users.Where(a => a.EMail == Username && a.Passwort == Password).FirstOrDefault();
            // User user = dc.User.Where(a => a.EMail == Username && a.Passwort == Password && a.AllowLogin && !a.Locked && !a.Deleted).FirstOrDefault();
            if (user != null)
            {
                dc.InsertLog("Login", string.Format("User: {0} erfolgreich angemeldet; Remote Addr.: {1}; Browser: {2}", user.EMail, remoteAdr, userAgent));
                return new ECOSoftWikiUser(user);
            }
            return null;
        }

        /// <summary>
        /// Sparpartuser über Art und ID
        /// </summary>
        /// <param name="dc"></param>
        /// <param name="userKind"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static ECOSoftWikiUser GetConsultingUser(ECOSoftWikiDataContext dc, string userID)
        {
            if (string.IsNullOrEmpty(userID)) return null;
            try
            {
                Guid uID = Guid.Parse(userID);
                Users user = dc.Users.Where(u => u.ID == uID).FirstOrDefault();
                if (user != null)
                {
                    return new ECOSoftWikiUser(user);
                }
            }
            catch { }
            return null;
        }

        #region private Konstruktoren 

        /// <summary>
        /// Konstrukoren sind privat -> Klasse kann nur über die statischen Methoden erstellt werden
        /// </summary>
        private ECOSoftWikiUser()
        {
            this.user = null;
        }

        private ECOSoftWikiUser(Users user)
        {
            this.user = user;
        }

        #endregion

        /// <summary>
        /// Um einen User wechsel zuzulassen
        /// </summary>
        public void Logoff(ECOSoftWikiDataContext dc)
        {
            if (IsAuthenticated) { dc.InsertLog("Logoff", string.Format("User: {0} abgemeldet", UserName)); }
            this.user = null;
            return;
        }

        public Users User
        {
            get { return user; }
        }

        public string UserID
        {
            get
            {
                if (User == null) { return null; }
                return User.ID.ToString();
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return (User != null);
            }
        }

        public string UserName
        {
            get
            {
                if (User != null) { return User.Name; }
                return null;
            }
        }

        public bool IsInRole(string role)
        {
            if (User != null)
            {
                switch (role)
                {
                    case ECOSoftWikiUserRole.Admin:
                        return user.Admin;
                    default:
                        return false;
                }
            }
            return false;
        }
        public bool IsInRole(string role1, string role2)
        {
            if (IsInRole(role1)) { return true; }
            if (IsInRole(role2)) { return true; }
            return false;
        }
        public bool IsInRole(string role1, string role2, string role3)
        {
            if (IsInRole(role1)) { return true; }
            if (IsInRole(role2)) { return true; }
            if (IsInRole(role3)) { return true; }
            return false;
        }
        public bool IsInRole(string role1, string role2, string role3, string role4)
        {
            if (IsInRole(role1)) { return true; }
            if (IsInRole(role2)) { return true; }
            if (IsInRole(role3)) { return true; }
            if (IsInRole(role4)) { return true; }
            return false;
        }
        public bool IsInRole(string role1, string role2, string role3, string role4, string role5)
        {
            if (IsInRole(role1)) { return true; }
            if (IsInRole(role2)) { return true; }
            if (IsInRole(role3)) { return true; }
            if (IsInRole(role4)) { return true; }
            if (IsInRole(role5)) { return true; }
            return false;
        }

        Users user;

    }

    public class ECOSoftWikiUserRole
    {
        public const string Admin = "Admin";
    }
}
