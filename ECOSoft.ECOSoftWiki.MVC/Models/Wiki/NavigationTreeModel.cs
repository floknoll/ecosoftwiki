﻿using ECOSoft.ECOSoftWiki.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOSoft.ECOSoftWiki.MVC.Models.Wiki
{
    public class NavigationTreeModel
    {
        public List<TreeViewModel> Branches { get; set; }
    }

    public class TreeViewModel
    {
        public TreeViewModel()
        {

        }
        public TreeViewModel(Branches b)
        {
            Branch = b;
        }
        public Branches Branch { get; set; }
    }
}