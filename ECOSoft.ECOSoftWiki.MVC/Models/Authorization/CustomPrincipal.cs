﻿using ECOSoft.ECOSoftWiki.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace ECOSoft.ECOSoftWiki.MVC.Models.Authorization
{
    public class CustomPrincipal : IPrincipal
    {
        public CustomPrincipal(CustomIdentity identity)
        {
            this.Identity = identity;
            this.CustIdentity = identity;
        }

        #region IPrincipal Members

        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role)
        {
            //if (CustIdentity != null && CustIdentity.User != null)
            //{
            //    return CustIdentity.User.IsInRole(role);
            //}
            return false;
        }

        #endregion

        public CustomIdentity CustIdentity { get; private set; }

    }

    public class CustomIdentity : IIdentity
    {

        public CustomIdentity(ECOSoftWikiUser user)
        {
            this.User = user;
        }

        #region IIdentity Members

        public string AuthenticationType
        {
            get { return "Custom"; }
        }

        public bool IsAuthenticated
        {
            get
            {
                //if (User != null) { return User.IsAuthenticated; }
                return false;
            }
        }

        public string Name
        {
            get
            {
                //if (User != null) { return User.UserName; }
                //else { return null; }
                return null;
            }
        }

        #endregion

        public ECOSoftWikiUser User { get; private set; }

    }

    public static class SessionPersister
    {
        // static string usernameSessionVar = "username";
        // static string useridSessionVar = new Guid().ToString();
        static string useridSessionVar = "userguid";
        static string userLanguageVar = "userlang";
        static string userTranslateVar = "userTrns";

        /// <summary>
        /// Der Username der in der Session festelegt wurde
        /// </summary>

        /*
        public static string Username
        {
            get
            {
                if (HttpContext.Current == null) return string.Empty;
                if (HttpContext.Current.Session[usernameSessionVar] != null)
                    return HttpContext.Current.Session[usernameSessionVar] as string;
                return null;
            }
            set { HttpContext.Current.Session[usernameSessionVar] = value; }
        }
        */

        public static string UserID
        {
            get
            {
                if (HttpContext.Current == null) return string.Empty;
                if (HttpContext.Current.Session[useridSessionVar] != null)
                    return HttpContext.Current.Session[useridSessionVar] as string;
                return null;
            }
            set { HttpContext.Current.Session[useridSessionVar] = value; }
        }

        public static string UserLang
        {
            get
            {
                if (HttpContext.Current == null) return string.Empty;
                if (HttpContext.Current.Session[userLanguageVar] != null)
                {
                    return HttpContext.Current.Session[userLanguageVar] as string;
                }
                return null;
            }
            set { HttpContext.Current.Session[userLanguageVar] = value; }
        }

        /// <summary>
        /// UserTranslate wird nur für die Sprachadmins benötigt. 
        /// Die Sprache die gerade übersetzt wird
        /// </summary>
        public static string UserTranslate
        {
            get
            {
                if (HttpContext.Current == null) return string.Empty;
                if (HttpContext.Current.Session[userTranslateVar] != null)
                {
                    return HttpContext.Current.Session[userTranslateVar] as string;
                }
                return null;
            }
            set { HttpContext.Current.Session[userTranslateVar] = value; }
        }


        /// <summary>
        /// Beendet die Session
        /// </summary>
        public static void Logoff()
        {
            HttpContext.Current.Session.Abandon();
        }
    }
}