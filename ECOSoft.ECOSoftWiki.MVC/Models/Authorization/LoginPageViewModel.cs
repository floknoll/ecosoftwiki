﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOSoft.ECOSoftWiki.MVC.Models.Authorization
{
    public class LoginPageViewModel
    {
        public LoginPageViewModel()
        {
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string Error { get; set; }

        // Texte für Anzeige

        #region Sprache und Texte für Anzeige
        /*
                public string Language { get; set; }

                public string Anmelden
                {
                    get { return DB.GetSeitentext("anmelden", "logon", Language, "Login"); }
                }

                public string Kontoinformationen
                {
                    get { return DB.GetSeitentext("kontoinformationen", "logon", Language, "Account information"); }
                }

                public string Email
                {
                    get { return DB.GetSeitentext("E-Mail", "logon", Language, "E-Mail"); }
                }

                public string Passwort
                {
                    get { return DB.GetSeitentext("passwort", "logon", Language, "Password"); }
                }

                public string Login
                {
                    get { return DB.GetSeitentext("login", "logon", Language, "Enter your E-Mail and Password."); }
                }
                */
        #endregion

        //private ConsultingDataContext db;
        //public ConsultingDataContext DB
        //{
        //    get
        //    {
        //        if (db == null) { db = new ConsultingDataContext(); }
        //        return db;
        //    }
        //}
    }
}