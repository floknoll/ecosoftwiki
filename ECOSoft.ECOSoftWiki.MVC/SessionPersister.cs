﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOSoft.ECOSoftWiki.MVC
{
    public class SessionPersister
    {
        static string useridSessionVar = "userguid";

        public static string UserID
        {
            get
            {
                if (HttpContext.Current == null) return string.Empty;
                if (HttpContext.Current.Session[useridSessionVar] != null)
                {
                    return HttpContext.Current.Session[useridSessionVar] as string;
                }
                return null;
            }
            set { HttpContext.Current.Session[useridSessionVar] = value; }
        }

        /// <summary>
        /// Beendet die Session
        /// </summary>
        public static void Logoff()
        {
            HttpContext.Current.Session.Abandon();
        }
    }
}