﻿using ECOSoft.ECOSoftWiki.MVC.Models.Wiki;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECOSoft.ECOSoftWiki.MVC.Controllers
{
    public class WikiController : BaseController
    {
        // GET: Wiki
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NavigationTree()
        {
            List<TreeViewModel> branches = new List<TreeViewModel>();

            branches = DC.Branches.OrderBy(a => a.Name).Select(s => new TreeViewModel(s)).ToList();

            return View(branches);
        }
    }
}