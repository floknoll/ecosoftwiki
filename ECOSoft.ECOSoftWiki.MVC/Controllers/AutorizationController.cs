﻿using ECOSoft.ECOSoftWiki.MVC.Models.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECOSoft.ECOSoftWiki.MVC.Controllers
{
    public class AutorizationController : BaseController
    {
        // GET: Autorization
        public ActionResult Login()
        {
            var vm = new LoginPageViewModel();
            ViewBag.isHome = false; // Masterpage für Startseite
            ViewBag.Menu = "Home"; // Masterpage für Menüslektion
            return View(vm);
        }

        [HttpPost]
        public ActionResult Login(LoginPageViewModel viewModel, string returnUrl)
        {
            if (string.IsNullOrEmpty(viewModel.Username) || string.IsNullOrEmpty(viewModel.Password))
            {
                viewModel.Error = "Bitte kontrollieren Sie E-Mail und Passwort";
                return View(viewModel);
            }
            string remoteAdr = Request.Params["REMOTE_ADDR"];
            string userAgent = Request.Params["HTTP_USER_AGENT"];

            //ConsultingUser user = ConsultingUser.Login(DC, viewModel.Username, viewModel.Password, remoteAdr, userAgent);
            //if (user == null)
            //{
            //    viewModel.Error = "Bitte kontrollieren Sie E-Mail und Passwort!";
            //    return View(viewModel);
            //}
            //SessionPersister.UserID = user.UserID;

            if (returnUrl != null && returnUrl.Length > 0)
            {
                return Redirect(returnUrl);
            }
            //if (user.User.Admin || user.User.Manager)
            //{
            //    return RedirectToAction("Index", "Stammdaten");
            //}
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logoff()
        {
            //if (VH.ConsultingUser != null) { VH.ConsultingUser.Logoff(DC); }
            SessionPersister.Logoff();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ReturnToLogin()
        {
            return RedirectPermanent("~/Home/Index");
        }
    }
}