﻿using ECOSoft.ECOSoftWiki.MVC.Models.Authorization;
using ECOSoft.ECOSoftWiki.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECOSoft.ECOSoftWiki.MVC.Controllers
{
    public class BaseController : Controller
    {
        /// <summary>
        /// Überschreiben damit Websiteverwaltungs-Tool nicht zugreift
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            string userID = SessionPersister.UserID;
            if (!CheckConnectionSSL()) { return; }
            ECOSoftWikiUser user = ECOSoftWikiUser.GetConsultingUser(DC, userID);
            if (user != null)
            {
                filterContext.HttpContext.User = new CustomPrincipal(new CustomIdentity(user));
            }
            base.OnAuthorization(filterContext);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            this.vh = new ViewHelper(Request, DC);
            ViewBag.ViewHelper = vh;
            ViewBag.isHome = false; // Masterpage für Startseite
            ViewBag.Menu = "Home"; // Masterpage für Menüslektion
            //if (!string.IsNullOrEmpty(Request.QueryString["lang"]))
            //{
            //   // SessionPersister.UserLang = Request.QueryString["lang"];
            //}
            //if (string.IsNullOrEmpty(SessionPersister.UserLang))
            //{
            //    SessionPersister.UserLang = determinePreveredLanguage();
            //}
            //this.vh.Language = SessionPersister.UserLang;
            //this.vh.Languages = DC.Sprachen.Where(s => s.Freigegeben).OrderBy(s => s.Reihung).Select(s => s.Sprache).ToArray();
            this.vh.AddBreadcrumb("/", "Home");
            determineLogin();
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            determineLogin();
            base.OnActionExecuted(filterContext);
        }

        /// <summary>
        /// Überprüft ob für sparepart.laska.at eine SSL Verbindung verwendet wird
        /// für alle anderen Verbindungen steht SSL nicht zur Verfügung!
        /// </summary>
        /// <returns></returns>
        protected bool CheckConnectionSSL()
        {
            if (Request.Url.Host == "sparepart.laska.at")
            {
                if (!Request.IsSecureConnection)
                {
                    Response.Redirect(Request.Url.AbsoluteUri.Replace("http:", "https:"));
                    return false;
                }
            }
            else // Da das Zertifikat am Server nur für die sparepart.laska.at URL
            {
                if (Request.IsSecureConnection)
                {
                    Response.Redirect(Request.Url.AbsoluteUri.Replace("https:", "http:"));
                    return false;
                }
            }
            return true;
            // Weitere Möglichkeit für Redirect lt. WEB
            // Methode um Parameter erweitern und Methode Result des Context verwenden
            // protected bool CheckConnectionSSL(AuthorizationContext filterContext)
            // filterContext.Result = new RedirectResult(...);
        }

        protected ECOSoftWikiDataContext DC
        {
            get
            {
                if (this.dc == null)
                {
                    this.dc = new ECOSoftWikiDataContext();
                }
                return this.dc;
            }
        }

        protected ViewHelper VH
        {
            get { return this.vh; }
        }

        private void determineLogin()
        {
            if (Request.IsAuthenticated)
            {
                CustomPrincipal cp = (CustomPrincipal)HttpContext.User;
                CustomIdentity ci = cp.CustIdentity;
                this.vh.ECOSoftWikiUser = cp.CustIdentity.User;
            }
            else
            {
                this.vh.ECOSoftWikiUser = null;
            }
        }

        private string determinePreveredLanguage()
        {
            try
            {
                // string lng = Request.Params["HTTP_ACCEPT_LANGUAGE"].ToString().Substring(0, 2);
                string lng = Request.Params["HTTP_ACCEPT_LANGUAGE"].Substring(0, 2);
                // var x = Request.UserLanguages;
                foreach (string l in this.vh.Languages)
                {
                    if (l.Equals(lng, StringComparison.OrdinalIgnoreCase))
                    {
                        return l;
                    }
                }
            }
            catch { }
            return "de"; // de wenn keine Sprache ermittelt werden konnte
        }

        ECOSoftWikiDataContext dc;
        ViewHelper vh;
    }
}