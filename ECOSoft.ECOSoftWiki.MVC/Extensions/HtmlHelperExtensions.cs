﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ECOSoft.ECOSoftWiki.MVC.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString ActionSubmitImage(this HtmlHelper html, object routeValues, string imagePath, string alt)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);

            // <img> tag
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imagePath));
            imgBuilder.MergeAttribute("alt", alt);
            imgBuilder.MergeAttribute("style", "border-color:White; border-width:0px; border-style:none; padding:0px; vertical-align:text-bottom");
            string imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

            // <a> tag
            var anchorBuilder = new TagBuilder("a");
            anchorBuilder.MergeAttribute("href", "Javascript:document.forms['form0'].submit();");
            anchorBuilder.InnerHtml = imgHtml; // include the <img> tag inside
            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }

        public static MvcHtmlString ActionImage(this HtmlHelper html, string action, object routeValues, string imagePath, string alt)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);

            // <img> tag
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imagePath));
            imgBuilder.MergeAttribute("alt", alt);
            imgBuilder.MergeAttribute("style", "border-color:White; border-width:0px; border-style:none; padding:0px; vertical-align:text-bottom");
            string imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

            // <a> tag
            var anchorBuilder = new TagBuilder("a");
            anchorBuilder.MergeAttribute("href", url.Action(action, routeValues));
            anchorBuilder.InnerHtml = imgHtml; // include the <img> tag inside
            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }

        public static MvcHtmlString ActionImage(this HtmlHelper html, string action, object routeValues, string imagePath, string alt, object htmlAttributes)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);

            // <img> tag
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imagePath));
            imgBuilder.MergeAttribute("alt", alt);
            imgBuilder.MergeAttribute("style", "border-color:White; border-width:0px; border-style:none; padding:0px; vertical-align:text-bottom");
            string imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

            // <a> tag
            var anchorBuilder = new TagBuilder("a");
            anchorBuilder.MergeAttribute("href", url.Action(action, routeValues));
            IDictionary<string, object> hlp = new RouteValueDictionary(htmlAttributes);
            anchorBuilder.MergeAttributes(hlp);
            anchorBuilder.InnerHtml = imgHtml; // include the <img> tag inside
            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }

        public static MvcHtmlString ActionImage(this HtmlHelper html, string action, string controller, object routeValues, string imagePath, string alt, object htmlAttributes)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);

            // <img> tag
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imagePath));
            imgBuilder.MergeAttribute("alt", alt);
            imgBuilder.MergeAttribute("style", "border-color:White; border-width:0px; border-style:none; padding:0px; vertical-align:text-bottom");
            string imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

            // <a> tag
            var anchorBuilder = new TagBuilder("a");
            anchorBuilder.MergeAttribute("href", url.Action(action, controller, routeValues));
            IDictionary<string, object> hlp = new RouteValueDictionary(htmlAttributes);
            anchorBuilder.MergeAttributes(hlp);
            anchorBuilder.InnerHtml = imgHtml; // include the <img> tag inside
            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }

        public static MvcHtmlString ActionInput(this HtmlHelper html, string action, object routeValues, string type, string value)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);
            var anchorBuilder = new TagBuilder("input");
            anchorBuilder.MergeAttribute("type", type);
            anchorBuilder.MergeAttribute("onclick", "javascript:location.href='" + url.Action(action, routeValues) + "'");
            anchorBuilder.MergeAttribute("value", value);
            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }

        public static MvcHtmlString ActionButton(this HtmlHelper html, string action, object routeValues, string type, string value)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);
            var anchorBuilder = new TagBuilder("input");
            anchorBuilder.MergeAttribute("type", type);
            anchorBuilder.MergeAttribute("onclick", "javascript:location.href='" + url.Action(action, routeValues) + "'");
            anchorBuilder.MergeAttribute("value", value);
            //anchorBuilder.InnerHtml = value;
            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }

        /// <summary>
        /// Liefert einen Javascript Link zurück um auf die letzte Seite in der History zukommen
        /// </summary>
        /// <returns></returns>
        public static MvcHtmlString ReturnLink()
        {
            var anchorBuilder = new TagBuilder("a");

            anchorBuilder.MergeAttribute("href", "Javascript:history.back();");
            //anchorBuilder.MergeAttribute("onclick", "");
            anchorBuilder.SetInnerText("Zurück");

            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }
    }
}