﻿using ECOSoft.ECOSoftWiki.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECOSoft.ECOSoftWiki.MVC
{
    public class ViewHelper
    {
        public ViewHelper(HttpRequestBase requ, ECOSoftWikiDataContext dc)
        {
            this.requ = requ;
            this.dc = dc;
            this.Breadcrumbs = new List<MvcHtmlString>();
            this.QuickLinks = new List<MvcHtmlString>();
            this.MenuTabs = new List<MvcHtmlString>();
        }

        /// <summary>
        /// Die aktuell ausgewählte Sprache
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Die möglichen Sprachen
        /// </summary>
        public string[] Languages { get; set; }

        public List<MvcHtmlString> Breadcrumbs { get; set; }
        public void AddBreadcrumb(string url, string text)
        {
            this.Breadcrumbs.Add(new MvcHtmlString(string.Format("<a href=\"{0}\" title=\"{1}\" onfocus=\"blurLink(this);\">{2}</a>", url, text, text)));
        }
        public void AddBreadcrumb(string action, string controller, string text)
        {
            this.AddBreadcrumb(string.Format("/{0}/{1}", controller, action), text);
        }
        public void AddBreadcrumbWithTitle(string url, string text, string title)
        {
            this.Breadcrumbs.Add(new MvcHtmlString(string.Format("<a href=\"{0}\" title=\"{1}\" onfocus=\"blurLink(this);\">{2}</a>", url, title, text)));
        }

        public List<MvcHtmlString> QuickLinks { get; set; }
        public void AddQuickLink(string url, string img, string text)
        {
            this.QuickLinks.Add(new MvcHtmlString(string.Format("<a href=\"{0}\" title=\"{2}\"><img src=\"{1}\" alt=\"{2}\"/></a>", url, img, text)));
        }
        // <a href="javascript:DownloadXLSX()" title="Download als Excel"><img src="/Images/document-excel.png" alt="Download als Excel"/></a>

        public List<MvcHtmlString> MenuTabs { get; set; }
        public void AddMenuTab(string url, string text, bool isActive)
        {
            string cls = isActive ? " class=\"active\"" : "";
            this.MenuTabs.Add(new MvcHtmlString(string.Format("<li{0}><a href=\"{1}\">{2}</a></li>", cls, url, text)));
        }

        #region User 

        public ECOSoftWikiUser ECOSoftWikiUser { get; set; }

        public bool IsAuthenticated
        {
            get { return ECOSoftWikiUser == null ? false : ECOSoftWikiUser.IsAuthenticated; }
        }

        public bool KeepAlive
        {
            get
            {
                if (IsAuthenticated) return true;
                return false;
            }
        }

        public Users User
        {
            get { return ECOSoftWikiUser == null ? null : ECOSoftWikiUser.User; }
        }

        #endregion

        #region Allgemein


        #endregion

        /// <summary>
        /// Erstellt eine URL für die aktuelle Seite, zum Ändern der Sprache
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public string GetLanguageUrl(string lang)
        {

            if (requ.QueryString.Count > 0)
            {
                string url = requ.Url.AbsoluteUri;
                string lng = requ.QueryString["lang"];
                if (string.IsNullOrEmpty(lng))
                {
                    // Sprache wird angefügt
                    return string.Format("{0}&{1}={2}", url, "lang", lang);
                }
                else
                {
                    // Sprache wird ersetzt
                    return url.Replace(string.Format("{0}={1}", "lang", lng), string.Format("{0}={1}", "lang", lang));
                }
            }
            else
            {
                // Sprache als ersten Parameter anfügen
                return string.Format("{0}?{1}={2}", requ.Url.AbsoluteUri, "lang", lang);
            }
        }


        HttpRequestBase requ;
        ECOSoftWikiDataContext dc;
    }
}