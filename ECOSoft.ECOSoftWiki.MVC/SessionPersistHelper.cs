﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECOSoft.ECOSoftWiki.MVC
{
    public class SessionPersistHelper
    {
        #region Konvertierung 

        private static int[] ArrayFromString(string val)
        {
            try
            {
                return val.Split(';').Select(n => Convert.ToInt32(n)).ToArray();
            }
            catch { }
            return new int[] { };
        }

        private static decimal[] DecArrayFromString(string val)
        {
            try
            {
                return val.Split(';').Select(n => Convert.ToDecimal(n)).ToArray();
            }
            catch { }
            return new decimal[] { };
        }

        private static string StringFromArray(int[] val)
        {
            try
            {
                return string.Join(";", val);
            }
            catch { }
            return null;
        }

        private static string StringFromDecArray(decimal[] val)
        {
            try
            {
                return string.Join(";", val);
            }
            catch { }
            return null;
        }

        #endregion

        #region Überprüfung 

        private static bool HasChaged(string alt, string neu)
        {
            if (alt == null && neu == null) { return false; }
            if (alt == null && neu != null) { return true; }
            if (alt != null && neu == null) { return true; }
            return !alt.Equals(neu);
        }

        private static bool HasChaged(int[] alt, int[] neu)
        {
            if (alt.Count() != neu.Count()) { return true; }
            return alt.Sum() != neu.Sum();
        }

        private static bool HasChaged(decimal[] alt, decimal[] neu)
        {
            if (alt.Count() != neu.Count()) { return true; }
            return alt.Sum() != neu.Sum();
        }

        private static bool HasChaged(Guid? alt, Guid? neu)
        {
            if (!alt.HasValue && !neu.HasValue) { return false; }
            if (alt.HasValue && !neu.HasValue) { return true; }
            if (!alt.HasValue && neu.HasValue) { return true; }
            return !alt.Value.ToString().Equals(neu.Value.ToString());
        }

        private static bool HasChaged(int? alt, int? neu)
        {
            if (!alt.HasValue && !neu.HasValue) { return false; }
            if (alt.HasValue && !neu.HasValue) { return true; }
            if (!alt.HasValue && neu.HasValue) { return true; }
            return alt.Value != neu.Value;
        }

        private static bool HasChaged(byte? alt, byte? neu)
        {
            if (!alt.HasValue && !neu.HasValue) { return false; }
            if (alt.HasValue && !neu.HasValue) { return true; }
            if (!alt.HasValue && neu.HasValue) { return true; }
            return alt.Value != neu.Value;
        }

        private static bool HasChaged(bool? alt, bool? neu)
        {
            if (!alt.HasValue && !neu.HasValue) { return false; }
            if (alt.HasValue && !neu.HasValue) { return true; }
            if (!alt.HasValue && neu.HasValue) { return true; }
            return alt.Value != neu.Value;
        }

        #endregion
    }
}